---

Plain HTML site to serve Prosperous Universe community data browsers.

Using GitLab Pages for deployment and serving.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
